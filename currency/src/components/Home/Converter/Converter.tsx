import React, {useContext, useEffect, useState} from 'react'
import BaseCtx from "../../../contexts/BaseCtx";
import {Button, Input} from "reactstrap";
import {FiRefreshCw} from "react-icons/fi"

type Props = {name: string, data: number}

const Converter:React.FC<Props> = (props) => {

    const base = useContext(BaseCtx);

    const [primary, setPrimary] = useState({name: base.base, data: 1})
    const [secondary, setSecondary]= useState({name: props.name, data: props.data})
    const [factor, setFactor] = useState(props.data)

    function handleAmountChange(e:any){
        setPrimary({name: primary.name, data: e.target.value});
    }

    function handleSwitch(){
        let p = primary
        let s = secondary
        setPrimary({name:s.name, data: s.data})
        setSecondary({name:p.name, data: p.data})
        setFactor(1/factor)
    }

    function convert(p:number,f:number){
        if(p*f >= 0.01){
            return Math.round(p*f*100)/100
        } else {
            return p*f
        }
    }

    useEffect(() => {
        console.log('effect used')
        setPrimary({name: base.base, data: 1})
        setSecondary({name: props.name, data: props.data})
        setFactor(props.data)
    },[props])

    return(
        <>
            <div style={{marginLeft:"auto", marginRight:"auto", textAlign:"center"}}>
                <Input value={primary.data} type="number" onChange={handleAmountChange} style={{display:"inline-block", width:"175px"}}/>
                <h2 style={{display:"inline-block"}}> {primary.name} = </h2>
                <h2 style={{display:"inline-block"}}>{primary.data && convert(primary.data,factor)} {secondary.name}</h2>
                <Button color="secondary" onClick={handleSwitch}><FiRefreshCw/></Button>
            </div>
        </>
    );
}

export default Converter