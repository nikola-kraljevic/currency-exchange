import { createContext } from "react"

const BaseCtx = createContext<{base:string, updateBase:(name:string)=>void}>({base:'EUR', updateBase:() => {}})

export default BaseCtx